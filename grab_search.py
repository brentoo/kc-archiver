import os,requests,math,json,copy,html,time,re,traceback
from datetime import datetime
from PIL import Image

import grab_server,settings


def searchTables(session,search_term,threadsList):

	page=""
	res2=""
	
	imgList=""
	resultsDis=0
	


	for a in threadsList:
		messageouter=str(threadsList[a])
		
		hit=False
		
		messageouter_message=str(threadsList[a]['message'])
		messageouter_subject=str(threadsList[a]['subject'])	
		messageouter_uri=str(threadsList[a]['boardUri'])	
		
		if search_term in messageouter_message or search_term in messageouter_subject:
			hit=True
			messageouter=grab_server.escapeMessage(threadsList[a]['message'],threadsList[a]['threadId'],threadsList[a]['boardUri'])
			imgList=grab_server.gatherThumbs(threadsList[a],imgList,False,True)
			res2+=f"<div id='post'><i><a href='/{messageouter_uri}/t/{threadsList[a]['threadId']}'>found in {threadsList[a]['threadId']}</a> ({threadsList[a]['creation']})<br><pre>{imgList}{messageouter}</pre></i></div><br>"
			
			imgList=""
		for b in (threadsList[a]['posts']):
			messageinner=str(b['message'])
			
			if search_term in messageinner:
				hit=True
				messageinner=grab_server.escapeMessage(b['message'],threadsList[a]['threadId'],threadsList[a]['boardUri'])
				imgList=grab_server.gatherThumbs(b,imgList,False,True)
				res2+=f"<div id='post'>found in <a href='/{messageouter_uri}/t/{threadsList[a]['threadId']}#{str(b['postId'])}'>{b['postId']}</a> ({b['creation']})<br><pre>{imgList}{messageinner}</pre></div><br>"


				
				resultsDis+=1
			imgList=""
					
			if resultsDis>5000:		
				break
				
		if hit==True:
			res2+=""
					
		if resultsDis>5000:
			res2+="Too many results"
			break
		
	arrPrep={}

	if 'wide' in session:
		if session['wide']==True:
			wideFormat='max-width:780px;'
		elif session['wide']==False:
			wideFormat=''
	else:
		wideFormat='max-width:780px;'
	
	if search_term=="":
		search_term="<i>-blank-</i>"
	
	arrPrep=f'''<table style="width:100%;margin-top:-5px;{wideFormat}font-size:10pt;"><tbody><tr><td style="text-align: left;width:26%;"><a href="/">↑ Home</a>'''+grab_server.generateSettings()+f'''       </td><td style="text-align:center;">Searched for <b>{search_term}</b></td><td style="text-align: right;width:35%;"><div class="boardList" style="max-width: 30%;">'''

	arrPrep+=f'<span>↷ /all/</span><div class="boardList-content">'

	for v in settings.temp_prefs:
	    	arrPrep+=f'<p><a href="/{v}">/{v}/</a></p>'
	    
	arrPrep+=f'</div></div> <form action="/search" method="get" style="display:inline;"><input style="width:61%;min-width:87.95px;font-size:10pt;max-width: 50%" type="text" placeholder="Search all boards" id="q" name="q"> <input style="font-size:10pt;" type="submit" value="Find"></form></td></tr></tbody></table><hr><div id="empty" style="padding-top:4px"></div>'


	
	page = '<!DOCTYPE html><head><title>KC-Archive</title><meta name = "viewport" content = "width = device-width"></head>'+grab_server.generatePageFormat(session)+arrPrep+page+res2

	
	return page
