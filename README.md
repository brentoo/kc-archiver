# KC-Archiver

Like the name implies, the KC-Archiver allows you to archive your favorite subboards of KC including the images (other attachments will come later). It also includes a small web server to view and search across all the subboards and threads, even without internet access.

## Features

* archives all the specified subboards including the posts into a single SQLite file
* downloads and converts the images to WebP (can be turned off)
* integrated web server to view the threads using static sites (no JS required)
* catalog and search function built-in

## Requirements

* running Tor instance (install the Tor package for your distribution)
* enough disk space (should be obvious)

## Installation

Prepare the archiver:
```
git clone https://gitlab.com/brentoo/kc-archiver.git
cd kc-archiver
python3 -m pip install -r requirements.txt
sudo apt update && sudo apt install uwsgi
sudo apt install uwsgi-plugin-python3
mkdir media && mkdir ./media/flags
```

Now edit the settings.py to specify the boards you want to archive and to change the Tor port, if necessary.

It's now possible to run the archiver by executing this command:
```
uwsgi --plugin python3 --http-socket :8250 --wsgi-file grab.py --enable-threads --callable app --thunder-lock
```

After running the archiver, you can reach the web interface by going to 'http://127.0.0.1:8250'. 

Enjoy! :)

## Questions?

If you have any questions, please ask in the thread, or open an issue here.