import requests,json,re,sqlite3,gc,os,sys,time,unicodedata,traceback,html,random

from lxml.html import fromstring
from flask import Flask,request,session,send_from_directory,render_template,send_file,redirect
from threading import Thread,Event
from datetime import datetime

import grab_server,grab_search,settings

s = requests.Session()
			
connection=sqlite3.connect(settings.filename, check_same_thread=False)
connection.text_factory = sqlite3.Binary
cursor=connection.cursor()


app = Flask(__name__)
app.config['SECRET_KEY']='ENTERSOMETHINGRANDOM'

app.template_folder = "."

app.url_map.strict_slashes = False


page=1
	

def fetchURL(session,url):
	while True:
		try:
			#print('Fetch '+url+' with '+str(proxies))
			res=session.get(url,proxies=settings.proxies,headers=settings.headers)
			#print(res)
			return json.loads(res.text)
		except:
			print("URL not available (trying again in 30 seconds...)")
			time.sleep(30)

def fetchThreads(c,conn):

			for v in settings.temp_prefs:

				imgList=""
				countBoard=0

				data=fetchURL(s,f'{settings.board}/{v}/catalog.json')

				data=sorted(data, key=lambda thread: thread['lastBump'],reverse=False)

				c.execute(f"SELECT id,zeit,antworten,gesichert,erreichbar FROM {v} where erreichbar = ?", (1,))
				result2=c.fetchall()

				for item in result2:
					if re.search('"threadId": '+str(item[0])+'', json.dumps(data), re.M):
					#	print(str(item[0])+" ist da!")
						continue
					else:
						print(str(item[0])+' has to go!')
						c.execute(f'''UPDATE {v} SET erreichbar = ? WHERE id = ? ''',('0',item[0]))
						conn.commit()
				
				c.execute(f"SELECT id,zeit,antworten,gesichert,erreichbar FROM {v} where erreichbar = ? and gesichert = ?", (0,0,))
				result2=c.fetchall()

				for item in result2:
					c.execute(f'''DELETE from {v} WHERE id = ? ''',(item[0],))
					conn.commit()
					print(str(item[0])+" removed!")

				for row in data:

					if 'message' in row:
						message=normalizeString(row['message'],'y',0,50,'utf-8')+'...' if len(row['message'])>50 else normalizeString(row['message'],'n',0,0,'utf-8')
						lastbump=normalizeString(row['lastBump'],'n',0,0,'utf-8')
						threadid=str(row['threadId'])
						if 'postCount' in row:
							row['postCount']+=1
							replies=str(row['postCount'])
						else:
							replies=1

				#		print message+'... ('+lastbump+') ('+threadid+') '+replies+' 0'
						try:
							c.execute(f"SELECT id,zeit,antworten,gesichert,erreichbar FROM {v} where id = ? LIMIT 1", (threadid,))
							result=c.fetchone()
						except:
							break
						if (result):
							if(result[0]==int(threadid)):
								#print('!!!'+str(result[0])+' ('+zeit+') '+str(result[2])+' | online: '+str(threadid)+' ('+str(lastbump)+') '+str(replies))
								if(result[2]!=int(replies)):
									while(True):
										try:
											c.execute(f'''UPDATE {v} SET zeit = ?,antworten = ? WHERE id = ? ''',(lastbump,replies,threadid))
											conn.commit()
											break
										except:
											print("Database can't be opened")
											break

								if(result[3]!=int(replies) and result[4]==True and countBoard<20):
									try:
										res=s.get(f'{settings.board}/{v}/res/'+threadid+'.json',proxies=settings.proxies,headers=settings.headers)
										tree = fromstring(res.content)
										if (tree.findtext('.//title'))=='File not found':
											print(threadid+" isn't available anymore")
											c.execute(f'''UPDATE {v} SET erreichbar = ? WHERE id = ? ''',('0',threadid))
											conn.commit()
										else: 
											data=json.loads(res.content)

											countBoard+=1

											imgList=grab_server.gatherThumbs(data,imgList)

											for b in reversed(data['posts']):
												imgList=grab_server.gatherThumbs(b,imgList,True)

											imgList=""
											
											try:
												lastchecked=data['posts'][-1]['creation']
											except:
												lastchecked=data['creation']
												

											c.execute(f'''UPDATE {v} SET voll = ?,gesichert = ?, zeit = ? WHERE id = ? ''',(res.content,replies,lastchecked,threadid))
											conn.commit()
											sys.stdout.write('\n'+threadid+' saved ('+str(round(time.time()))+')')
											sys.stdout.flush()
											
											sys.stdout.write("\r\n")
											sys.stdout.write(f"{v} refreshed ("+str(round(time.time()))+")")
											sys.stdout.flush()
											
											

									except Exception:
										print(traceback.format_exc())
										print('Error (trying again...)')
										break

							continue
						else:
							try:
								print ('Add (',cMessage(message),', ',threadid,')')
								sys.stdout.flush()
								conn.execute(f"INSERT INTO {v} VALUES (?, ?, ?, ?, ?, '0', '1')", (message,message,lastbump,threadid,replies,))
								conn.commit() 
							except:
								break

				

				
				print("\nplease wait...")
				time.sleep(30)
				print("\nrefresh")


def createTable(c,conn):
	c.execute('''SELECT tbl_name FROM sqlite_master WHERE type='table'
	''')
	liste=c.fetchall()
	
	for v in settings.temp_prefs:
		if v in liste:
			continue
		else:
			c.execute(f'''CREATE TABLE {v} (nachricht text,voll text,zeit text,id number,antworten number,gesichert number,erreichbar number)''')
	conn.commit()



def cMessage(value):
    return ''.join(value.splitlines())



def normalizeString(array,cut,start,stop,encoding):
	if (cut)=='y': 
		normal=bytes(unicodedata.normalize('NFKD',array)[start:stop].encode(encoding,'ignore'))
		return normal.decode('utf-8')
	else: 
		normal=bytes(unicodedata.normalize('NFKD',array).encode(encoding,'ignore'))
		return normal.decode('utf-8')
	


@app.route('/<bid>/t/<fid>')
def doThread(fid,bid):
	faden = fid

	cursor=connection.cursor()

	cursor.execute(f'''SELECT COUNT(*) FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert''')
	tsNum=cursor.fetchone()[0]

	cursor.execute(f'''SELECT "_rowid_",* FROM "{bid}" WHERE id={faden} ''')
	tsRec=cursor.fetchall()

	threadPage={}

	for a in range(0,len(tsRec)):
		for b in tsRec[a]:
			if (type(b)==bytes):
				threadPage[a]=bytes(b)
		threadPage[a]=json.loads(threadPage[a])

	response=grab_server.showThread(tsNum,threadPage,page,bid)
	return response


@app.route('/<bid>')
def doServer(bid):
	cursor=connection.cursor()

	cursor.execute(f'''SELECT COUNT(*) FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert''')
	tsNum=cursor.fetchone()[0]

	cursor.execute(f'''SELECT "_rowid_",* FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert ORDER BY "zeit" DESC LIMIT 0, 10 ''')
	tsRec=cursor.fetchall()

	#print(firstThread)

	threadsList={}

	for a in range(0,len(tsRec)):
		for b in tsRec[a]:
			if (type(b)==bytes):
				threadsList[a]=bytes(b)
		threadsList[a]=json.loads(threadsList[a])


	response=grab_server.sendList(tsNum,threadsList,page,bid)
	return response
	
	

@app.route('/<bid>/p/<pid>')
def doPage(bid,pid):
	page = int(pid)

	pageMin=page*10-10

	cursor=connection.cursor()

	cursor.execute(f'''SELECT COUNT(*) FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert''')
	tsNum=cursor.fetchone()[0]

	cursor.execute(f'''SELECT "_rowid_",* FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert ORDER BY "zeit" DESC LIMIT {pageMin},10 ''')
	tsRec=cursor.fetchall()

	#print(firstThread)

	threadsList={}

	for a in range(0,len(tsRec)):
		#print('Länge'+str(len(tsRec)))
		for b in tsRec[a]:
			if (type(b)==bytes):
				threadsList[a]=bytes(b)
		threadsList[a]=json.loads(threadsList[a])


	response=grab_server.sendList(tsNum,threadsList,page,bid)
	return response
	
	
@app.route('/<bid>/catalog')
def doCatalog(bid):

	cursor=connection.cursor()

	cursor.execute(f'''SELECT COUNT(*) FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert''')
	tsNum=cursor.fetchone()[0]

	cursor.execute(f'''SELECT "_rowid_",* FROM "{bid}" WHERE gesichert > 0 AND voll!=nachricht AND antworten==gesichert ORDER BY "zeit" DESC ''')
	tsRec=cursor.fetchall()

	#print(firstThread)

	threadsList={}

	for a in range(0,len(tsRec)):
		#print('Länge'+str(len(tsRec)))
		for b in tsRec[a]:
			if (type(b)==bytes):
				threadsList[a]=bytes(b)
		threadsList[a]=json.loads(threadsList[a])


	response=grab_server.sendList(tsNum,threadsList,1,bid,True)
	return response



@app.route('/')
def getHome():
	return redirect("/"+settings.temp_prefs[0], code=302)
	


@app.route('/search', methods=['GET'])
def doSearch():
	#bid=request.form['fbid']
	search_term=request.args.get('q')
	
	cursor=connection.cursor()

	cursor.execute("SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%'")
	rows = cursor.fetchall()
	
	boards = []
	
	for row in rows:
		json_like_memoryview = row[0]
		json_like_bytes = bytes(json_like_memoryview)  # Convert memoryview to bytes
		json_like_string = json_like_bytes.decode('utf-8')  # Convert bytes to str
		boards.append(json_like_string)
	
	query1=""

	for a in range(0,len(boards)):
		if a==0:
			query1+=f'SELECT "voll","zeit" FROM "{boards[a]}" '
		else:
			query1+=f'UNION SELECT "voll","zeit" FROM "{boards[a]}" '
			
		if a==(len(boards)-1):
			query1+=f'ORDER BY "zeit" '

	#return(query1)

	cursor.execute(query1)
	rows = cursor.fetchall()

	results = 0

	threadsList={}

	matching_rows = []
	

	for row in rows:
		json_like_memoryview = row[0]
		json_like_bytes = bytes(json_like_memoryview)  # Convert memoryview to bytes
		json_like_string = json_like_bytes.decode('utf-8')  # Convert bytes to str

		try:
			threadsList[results]=json.loads(json_like_string)
			
		except Exception as e:
			print(e)
			
		results+=1
		
	
	#threadsList = sorted(threadsList, key=lambda k: threadsList[k]['message'], reverse=True)	
	
	
	
	response=grab_search.searchTables(session,search_term,threadsList)
	return response
	

@app.route('/settings', methods=['POST'])
def setPref():
	if 'fwidth' in request.form:
		if 'wide' in session:
			session['wide']=True
	else:
		session['wide']=False
		
	if 'fcompress' in request.form:
		settings.compress_pref=True
	else:
		settings.compress_pref=False

	return redirect(request.headers.get("Referer"), code=302)


@app.route('/media/<fn>')
def sendImage(fn):
	try:
		return send_file(f'./media/{fn}')
	except Exception as e:
		return str(e)


@app.route('/media/flags/<fn>')
def sendFlag(fn):
	try:
		return send_file(f'./media/flags/{fn}')
	except Exception as e:
		return str(e)

@app.route('/favicon.ico')
def sendFavicon():
	return send_file(f'./favicon.ico')


@app.errorhandler(500)
def internal_server_error(error):
    return f'This didn\'t work.<br><a href="/">Back to home</a>', 500


thread=Thread()
thread_stop_event=Event()

class TimerThread(Thread):

	def __init__(self,inst):
		super(TimerThread,self).__init__()


	def run(self):
		global cursor,connection

		while(True):
			if os.path.exists(settings.filename) and os.path.getsize(settings.filename) > 0:
				random.shuffle(settings.temp_prefs)
				fetchThreads(cursor,connection)
				#print('freeing up memory...')
				gc.collect()
			else:
				connection=sqlite3.connect(settings.filename, check_same_thread=False)
				connection.text_factory = sqlite3.Binary
				cursor=connection.cursor()
				createTable(cursor,connection) # for all prefs	



class Session:

	def __init__(self,inst):
		print('init')
		



if not thread.is_alive():
	print("Starting Thread")
	first=Session('Main')
	thread=TimerThread(first)
	thread.start()






