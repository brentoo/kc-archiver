# The current onion URL of KC.
board='http://kohlchanvwpfx6hthoti5fvqsjxgcwm3tmddvpduph5fqntv5affzfqd.onion'

# This .sql file will contain all the boards specified below including the posts, but without the media files.
filename='listnew.sql'

# Please specify the boards you want to archive. 
# If the .sql file doesn't exist, it will create it and the appropriate tables inside for each board. 
# This shouldn't be changed later on; instead use a separate file for a different array of boards.
temp_prefs=['a','int','b']

# By default the archiver converts .png images to .webp, which saves a lot of space on your disk, but you can instead also save the original .png file.
compress_pref=True

# In order to use the archiver, a Tor instance has to be running. 
# If not, you won't be able to reliably get around the CDN. You can change the Tor port, if necessary.
proxies = {
	'http': 'socks5h://127.0.0.1:9050',
	'https': 'socks5h://127.0.0.1:9050'
}

# This shouldn't be changed.
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
}

